# README #

En este Readme voy a estar escribiendo algunos apuntes de clase.

## Semana 1 ##

## Migración ##

Comencé y tuve éxito en la migración de Nativescript de la versión 6.5.1 a la 7.0.11. Modifiqué y actualicé varias librerías y tuve que refactorizar la importación de varias librerías.
### Comandos ###

npm install -g nativescript

tns migrate

* [Extraído de](https://docs.nativescript.org/releases/upgrade-instructions)


## Primeros pasos ##

Al principio tuve muchos problemas, daba muchos errores. El problema principal es que me instalaba de forma predeterminada la últma versión
Hoy 25/10/200 es la 7.11
Lo solucioné intalando en forma manual una versión más antigua, la 6.5.1 de Nativescript
sudo npm install -g nativescript@6.5


## Crear componente ##

Instalación previa:

* npm i --save-dev @schematics/angular
* npm i --save-dev @angular/cli
* npm i --save-dev @nativescript/schematics

Luego: 

Desde la ruta /src/app/ utilicé:
ng generate component noticias --proyect=my-drawer-ng --skip-import
ng generate component votos --proyect=my-drawer-ng --skip-import


### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)
