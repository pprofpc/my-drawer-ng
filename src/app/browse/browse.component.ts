import { Component, OnInit, ViewChild } from "@angular/core";

import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Application, makeParser } from "@nativescript/core";
import { ElementRef } from "@angular/core";
import { registerElement } from "@nativescript/angular/";

registerElement("MapView", () => require("nativescript-google-maps-sdk").MapView);
//var gmaps = require ("nativescript-google-maps-sdk");
var mapsModule = require("nativescript-google-maps-sdk");

@Component({
    selector: "Browse",
    templateUrl: "./browse.component.html"
})
export class BrowseComponent implements OnInit {
    @ViewChild("MapView") mapView: ElementRef;

    constructor() {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        // Init your component properties here.
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>Application.getRootView();
        sideDrawer.showDrawer();
    }

    // Map events
    onMapReady(args) {
    var mapView = args.object;

    console.log("Setting a marker...");
    var marker = new mapsModule.Marker();
        marker.position = mapsModule.Position.positionFromLatLng(-34.6037, -58.3817);
    marker.title = "Buenos Aires";
    marker.snippet = "Argentina";
    marker.userData = { index: 1 };
    mapView.addMarker(marker);

    // Disabling zoom gestures
    //mapView.settings.zoomGesturesEnabled = false;
}
}
