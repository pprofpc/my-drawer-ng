import { Injectable } from "@angular/core";
import { ApplicationSettings } from "@nativescript/core";
import { getJSON } from "@nativescript/core/http";
import { request } from "@nativescript/core/http";
const sqlite = require("nativescript-sqlite");

@Injectable()
export class NoticiasService {
  api: string = ApplicationSettings.getString("url");
  favoritosList: Array<any>;
  private database: any; //sqlite abierta


  constructor() {
    this.getDb((db) => {
      console.dir(db);
      db.each("select * from fav  ",
        (err, fila) => console.log("fila: ", fila),
        (err, totales) => console.log("Filas totales: ", totales));
    }, () => console.log("error on getDB"));


  }

  getDb(fnOk, fnError) {
    return new sqlite("mi_db_fav", (err, db) => {
      if (err) {
        console.error("Error al abrir db!", err);
      } else {
        console.log("Está la db abierta: ", db.isOpen() ? "Si" : "No");
        this.database=db;
        db.execSQL("CREATE TABLE IF NOT EXISTS fav (id INTEGER PRIMARY KEY AUTOINCREMENT, favorito TEXT)")
          .then((id) => {
              console.log("CREATE TABLE OK");
              fnOk(db);
          }, (error) => {
              console.log("CREATE TABLE ERROR", error);
              fnError(error);
          });
      }
    });
  }

  public getdbConnection() {
    return new sqlite('mi_db_fav');
  }

  agregarFavorito(f: string){
    this.getDb((db) => {
      db.execSQL("insert into fav (favorito) values (?)", [f],
        (err, id) => console.log("nuevo id: ", id));
    }, () => console.log("error on getDB"));
    
    return request({
      url: this.api + "/favs",
      method: "POST",
      headers: { "Content-Type": "application/json" },
      content: JSON.stringify({
        nuevo: f
      })
    });
    /*
    this.getDb((db) => {
      db.execSQL("select favorito from fav where id=*",
        (err, favorito) => console.log("Favoritos: ", favorito));
    }, () => console.log("error on getDB"));
*/
  }

  /*
  getFavoritos() {
    this.getDb((db) => {
      db.execSQL("select favorito from fav",
        (err, filas) => { 
            for (var row in filas) {
              this.favoritosList.push({
                id: filas[row][0],
                name: filas[row][1]
              });
        }}, err => {
          console.log("error en la consulta: " + err);
        });
    }, () => console.log("error on getDB"));
  }
*/

//Info disponible en: https://www.thepolyglotdeveloper.com/2016/10/using-sqlite-in-a-nativescript-angular-2-mobile-app/
  getFavoritos(){
    console.log("Estoy en getFavoritos!");
    this.favoritosList = [];
    this.getDb((db) => {
      this.database.all("SELECT * FROM fav").then(rows => {
           for (var row in rows){
             this.favoritosList.push({
               //id: rows[row][1], 
               "name": rows[row][1]
              });
          }
            }, error => {
              console.log("error en la consulta: "+ error);
            });
    }, () => {
      console.log("error no conecto");
    })
    return getJSON(this.api + "/favs");
  }


  setFavorito(f: string){
    console.log("A cargar: "+f);
    this.favoritosList.push(f[1]);

  }

  agregar(s: string) {
    return request({
      url: this.api + "/favs",
      method: "POST",
      headers: { "Content-Type": "application/json" },
      content: JSON.stringify({
          nuevo: s
      })
    });
  }

  setApi():boolean{
    if (ApplicationSettings.getString("url")!=null){
    this.api = ApplicationSettings.getString("url");
    return true;}
    else return false;
  }

  favs() {
    return getJSON(this.api + "/favs");
  }

  buscar(s: string) {
    if(this.setApi){
     return getJSON(this.api + "/get?q=" + s);
    }
  }
}
