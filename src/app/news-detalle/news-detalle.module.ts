import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "@nativescript/angular";
import { NoticiasService } from "../domain/noticias.service";

import { NewsDetalleRoutingModule } from "./news-detalle-routing.module";
import { NewsDetalleComponent } from "./news-detalle.component";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        NewsDetalleRoutingModule
    ],
    declarations: [
        NewsDetalleComponent
    ],
    //providers: [NoticiasService],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class NewsDetalleModule { }
