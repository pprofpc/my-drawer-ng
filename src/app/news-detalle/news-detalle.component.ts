import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { RouterExtensions } from "@nativescript/angular";
import { Application, ApplicationSettings } from "@nativescript/core";
import { Router } from "@angular/router";
import { NoticiasService } from "../domain/noticias.service";
import { stringify } from "@angular/compiler/src/util";
import * as Toast from "nativescript-toast";

@Component({
  selector: 'ns-news-detalle',
  templateUrl: './news-detalle.component.html',
  styleUrls: ['./news-detalle.component.tns.css']
})
export class NewsDetalleComponent implements OnInit {
  votos = 3;
  texto: string;
  noticiaTitulo: string;
  noticiaTexto: string;
  constructor(public noticias: NoticiasService, private router: Router, private routerExtensions: RouterExtensions) {
    this.noticiaTexto = ApplicationSettings.getString("sePide");
    this.texto = "Noticia 1: Es la primer noticia sobre " + this.noticiaTexto;

   }

  onButton1Tap(navItemRoute: string): void {
    this.noticias.agregarFavorito(this.noticiaTexto);

    console.log("Favorito: "+ this.noticiaTexto);

    this.routerExtensions.navigate([navItemRoute], {
      transition: {
        name: "fade"
      }
    });
  }

  getTitulo(titulo: string=""): string {
     titulo= this.texto.split(":")[0];
     return titulo;
   }
  getTexto(texto: string = ""): string {
    texto = this.texto.split(":")[1];
    return texto;
  }


  ngOnInit(): void {

  }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>Application.getRootView();
        sideDrawer.showDrawer();
    }

    onItemTap(x): void {
        console.dir(x);
    }

    onTap(voto):void{
      if (voto =="votoUp"){
        this.votos +=1;
      }
      else {
        this.votos -= 1;
      }
      console.log('Votos: '+this.votos);
      const toastMensaje = Toast.makeText("Gracias por votar!!", "short");
      toastMensaje.show();
    }
    
    getVotos(): string{
      return ("Votos: "+this.votos);
    }

    onButtonTap(navItemRoute: string): void {
      this.routerExtensions.navigate([navItemRoute], {
        transition: {
          name: "fade"
        }
      });
    }
}

