import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "@nativescript/angular";

import { NewsDetalleComponent } from "./news-detalle.component";

const routes: Routes = [
    { path: "", component: NewsDetalleComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class NewsDetalleRoutingModule { }
