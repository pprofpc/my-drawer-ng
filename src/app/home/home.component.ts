import { Component, OnInit } from "@angular/core";
import { ImageSource } from "@nativescript/core/image-source";
import { Image } from "@nativescript/core/ui/image";
import { isAvailable, requestCameraPermissions, takePicture } from '@nativescript/camera';
import * as SocialShare from "@nativescript/social-share";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Application } from "@nativescript/core";

@Component({
    selector: "Home",
    templateUrl: "./home.component.html"
})
export class HomeComponent implements OnInit {

    constructor() {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        // Init your component properties here.
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>Application.getRootView();
        sideDrawer.showDrawer();
    }

    /*
    onButtonTap(): void {
        requestCameraPermissions().then(
            function success() {
                const options = { width: 300, height: 300, keepAspectRatio: false, saveToGallery: true };
                takePicture(options).
                    then((imageAsset) => {
                        console.log("Size: " + imageAsset.options.width + "x" + imageAsset.options.height);
                        console.log("keepAspectRatio: " + imageAsset.options.keepAspectRatio);
                        console.log("Photo saved in Photos/Gallery for Android or in Camera Roll for iOS");

                        //const imgPhoto = new ImageSource();
                        var bstring :string;
                        const that = this;
                        //this.imgPhoto.fromAsset(imageAsset);

                        ImageSource.fromAsset(imageAsset).then((imgSrc) => {
                            if (imgSrc) {

                                // This is the base64 string, I saved it to a global variable to be used later
                                bstring = imgSrc.toBase64String("png");

                                this.imgPhoto.fromBase64(bstring);

                                console.log(bstring);

                                //SocialShare.shareImage(imgSrc, "Asunto: compartido desde el curso!");

                                // This bit saves it as an 'Image' to the app
                                const fs = require("file-system");
                                const mil = new Date().getTime();
                                const folder = fs.knownFolders.documents();
                                const path = fs.path.join(folder.path, bstring);
                                const saved = this.imgPhoto.saveToFile(path, "png");

                                // This saves the image to the global variable which will be used to display it on the screen
                                //that.saveImage = path;
                                //that.picHeight = imgSrc.height;
                                
                                console.log("Foto: " + path );

                            } else {
                                alert("Image source is bad.");
                            }
                        });
                    }).catch((err) => {
                        console.log("Error -> " + err.message);
                    });
                        
        });
    }
    */
    
    onButtonTap(): void {
        requestCameraPermissions().then(
            function success() {
                const options = { width: 300, height: 300, keepAspectRatio: false, saveToGallery: true };
                takePicture(options).
                    then((imageAsset) => {
                        console.log("Tamaño: " + imageAsset.options.width + "x" + imageAsset.options.height);
                        console.log("keepAspectRatio: " + imageAsset.options.keepAspectRatio);
                        console.log("Foto guardada!");
                        console.log("Toda la data: " + JSON.stringify(imageAsset));
                        console.log("Ruta: "+ imageAsset.android);
                        ImageSource.fromAsset(imageAsset)
                            .then((imageSource) => {
                                console.log("Imagen para RS: " + imageSource.android);
                                SocialShare.shareImage(imageSource , "Asunto: compartido desde el curso!");
                            }).catch((err) => {
                                console.log("Error 1 -> " + err.message);
                            });
                    }).catch((err) => {
                        console.log("Error 2-> " + err.message);
                    });
            },
            function failure() {
                console.log("Permiso de camara no aceptado por el usuario");
            }
        );
    }
}

