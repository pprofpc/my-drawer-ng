import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Application, GestureEventData } from "@nativescript/core";
import { GridLayout } from "@nativescript/core/ui/layouts/grid-layout";
import * as Toast from "nativescript-toast";
import {
    connectionType,
    getConnectionType,
    startMonitoring,
    stopMonitoring
} from "@nativescript/core/connectivity";
import { Device, Screen } from "@nativescript/core/platform";
import * as dialogs from "@nativescript/core/ui/dialogs";
import { ApplicationSettings } from "@nativescript/core";

@Component({
    selector: "Featured",
    templateUrl: "./featured.component.html"
})
export class FeaturedComponent implements OnInit {
    monitoreando: boolean = false;
    android: string;

    constructor() {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        // Init your component properties here.
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>Application.getRootView();
        sideDrawer.showDrawer();
    }

    onLongPress(args: GestureEventData) {
        console.log("Object that triggered the event: " + args.object);
        console.log("View that triggered the event: " + args.view);
        console.log("Event name: " + args.eventName);

        const grid = <GridLayout>args.object;
        grid.rotate = 0;
        grid.animate({
            rotate: 360,
            duration: 2000
        });
    }
 
    onDatosPlataforma(): void {
        console.log("modelo", Device.model);
        console.log("tipo dispositivo", Device.deviceType);
        console.log("Sistema operativo", Device.os);
        console.log("versión sist operativo", Device.osVersion);
        console.log("Versión sdk", Device.sdkVersion);
        console.log("lenguaje", Device.language);
        console.log("fabricante", Device.manufacturer);
        console.log("código único de dispositivo", Device.uuid);
        console.log("altura en pixels normalizados", Screen.mainScreen.heightDIPs); // DIP (Device Independent Pixel), también conocido como densidad de píxeles independientes.Un píxel virtual que aparece aproximadamente del mismo tamaño en una variedad de densidades de pantalla.
        console.log("altura pixels", Screen.mainScreen.heightPixels);
        console.log("escala pantalla", Screen.mainScreen.scale);
        console.log("ancho pixels normalizados", Screen.mainScreen.widthDIPs);
        console.log("ancho pixels", Screen.mainScreen.widthPixels);

        const toastMensaje = Toast.makeText((Device.deviceType) + ", " + Device.os + ", SDK: " + Device.sdkVersion, "short");
        toastMensaje.show();
    }

    onMonitoreoDatos(): void {
        const myConnectionType = getConnectionType();
        switch (myConnectionType) {
            case connectionType.none:
                console.log("Sin Conexion");
                break;
            case connectionType.wifi:
                console.log("WiFi");
                break;
            case connectionType.mobile:
                console.log("Mobile");
                break;
            case connectionType.ethernet:
                console.log("Ethernet"); // es decir, cableada
                break;
            case connectionType.bluetooth:
                console.log("Bluetooth");
                break;
            default:
                break;
        }
        this.monitoreando = !this.monitoreando;
        if (this.monitoreando) {
            startMonitoring((newConnectionType) => {
                switch (newConnectionType) {
                    case connectionType.none:
                        console.log("Cambió a sin conexión.");
                        break;
                    case connectionType.wifi:
                        console.log("Cambió a WiFi.");
                        break;
                    case connectionType.mobile:
                        console.log("Cambió a mobile.");
                        break;
                    case connectionType.ethernet:
                        console.log("Cambió a ethernet.");
                        break;
                    case connectionType.bluetooth:
                        console.log("Cambió a bluetooth.");
                        break;
                    default:
                        break;
                }
            });
        } else {
            stopMonitoring();
        }
    }

    onButtonCorreo() {
        let options: dialogs.PromptOptions = {
            title: "Correo de usuario",
            inputType: dialogs.inputType.text,
            defaultText: "username@mail.com",
            okButtonText: "Guardar",
            cancelButtonText: "Cancelar",
            cancelable: true
        }

        dialogs.prompt(options).then((result: dialogs.PromptResult) => {
            if (result.result === true) {
                if (result.text.length > 0) {
                    ApplicationSettings.setString("userMail", result.text);
                    console.log("userMail: " + ApplicationSettings.getString("userMail"));
                }
            }
        })
    }

}
