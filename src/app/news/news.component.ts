import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { RouterExtensions } from "@nativescript/angular";
import { Application, ApplicationSettings, Color, Folder, knownFolders, View } from "@nativescript/core";
import { fromUrl, ImageSource } from "@nativescript/core/image-source";
import { NavigationExtras, Router } from "@angular/router";
import { NoticiasService } from "../domain/noticias.service";
import { stringify } from "@angular/compiler/src/util";
import { Noticia, NuevaNoticiaAction } from "../domain/noticias-state.model";
import * as Toast from "nativescript-toast";
import * as SocialShare from "@nativescript/social-share";
import { AppState } from "../app.module";
import { Store } from "@ngrx/store";

@Component({
  selector: 'ns-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.tns.css']
})
export class NewsComponent implements OnInit {
  resultados: Array<string> = [];
  folder: Folder = <Folder>knownFolders.currentApp();
  @ViewChild("layout") layout: ElementRef;
  ruta: string = "https://pbs.twimg.com/profile_images/569917785539485696/z4Nuc_Mt.png";


  constructor(public noticias: NoticiasService, private router: Router, private routerExtensions: RouterExtensions,
      private store: Store<AppState>
      ) {
  
   }

  doLater(fn) { setTimeout(fn, 1000); }

  ngOnInit(): void {
        this.store.select((state) => state.noticias.sugerida)
            .subscribe((data) => {
                const f = data;
                if (f != null) {
                  const toastMensaje = Toast.makeText("Sugerimos Leer: " + f.titulo, "short");
                  this.doLater(() => toastMensaje.show());
                }
            });
    //Ahora lo consumimos desde el API
    /*console.log("Estoy en: "+this.folder.path);
    this.noticias.agregar("Noticia 1");
    this.noticias.agregar("Noticia 2");
    this.noticias.agregar("Noticia 3");*/
  }

  onPull(e) {
    const sNews  = "Noticia Nueva";
    console.log("e: "+e);
    const pullRefresh = e.object;
    setTimeout(() => {
      //this.resultados.push("xxxxxxx");
      this.noticias.agregar(sNews);
      pullRefresh.refreshing = false;
    }, 2000);
    this.store.dispatch(new NuevaNoticiaAction(new Noticia(sNews)));

  }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>Application.getRootView();
        sideDrawer.showDrawer();
    }

    onItemTap(x): void {
      
      let destino= "/detalle";
      this.routerExtensions.navigate([destino], {
        transition: {
          name: "fade"
        }
      });
      console.dir(x);      
    }
    
  onLongPressImg(): void {
    console.log("Estoy en LongPress: ");
    //let image = ImageSource.fromUrl(this.ruta);
    //SocialShare.shareImage(image);
    //const img: ImageSource = <ImageSource><unknown>fromUrl(this.ruta);
    //console.log(img);

    ImageSource.fromUrl(this.ruta)
      .then((imageSource) => {
        console.log("Imagen para RS: " + imageSource.height);
        SocialShare.shareImage(imageSource, "Asunto: compartido desde el curso!");
      }).catch((err) => {
        console.log("Error 1 -> " + err.message);
      });
  }

    onLongPress(s): void {
      console.log(s);
      SocialShare.shareText(s, "Asunto: compartido desde el curso!");
    }

    onItemTapFavorito(f): void{
      this.noticias.agregarFavorito(f);
      const toastMensaje = Toast.makeText(f + " agregado a Favoritos!", "short");
      toastMensaje.show();
    }

    onButtonTap(navItemRoute: string): void {
      this.routerExtensions.navigate([navItemRoute], {
        transition: {
          name: "fade"
        }
      });
    }

    buscarAhora(s: string){
      console.dir("buscarAhora: " + s);
      this.noticias.buscar(s).then((r: any) =>{
        console.log("resultados buscarAhora: " + JSON.stringify(r));
        this.resultados = r;
        const layout = <View>this.layout.nativeElement;
        layout.animate({
          backgroundColor: new Color("blue"),
          duration: 300,
          delay: 150
        }).then(() => layout.animate({
          backgroundColor: new Color("white"),
          duration: 300,
          delay: 150
        }));
      }, (e) =>{
        console.log("error buscarAhora "+ e);
        Toast.makeText("Error en la búsqueda","short").show;
      });
    }

  onNavItemTap(navItemRoute: string, y: string): void {
    ApplicationSettings.setString("sePide", y);
    /*
    let navigationExtras: NavigationExtras = {
      queryParams: {
        "tema:" : "Cocina",
        "info" : "Nuevo Mexico"
      }
    }
    console.dir("Parametro: " + navigationExtras.queryParams);
    
    console.dir(navigationExtras.queryParams);
*/
    this.router.navigate([navItemRoute]);
  }

}
/*Info: https://www.thepolyglotdeveloper.com/2016/10/passing-complex-data-angular-2-router-nativescript/ */
