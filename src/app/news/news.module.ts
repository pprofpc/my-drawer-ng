import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule, NativeScriptFormsModule } from "@nativescript/angular";
import { NoticiasService } from "../domain/noticias.service";

import { NewsFormComponent } from "./news-form.component";

import { NewsRoutingModule } from "./news-routing.module";
import { NewsComponent } from "./news.component";

import {MinLenDirective} from "../minilen.validator";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        NewsRoutingModule,
        NativeScriptFormsModule
    ],
    declarations: [
        NewsComponent,
        NewsFormComponent,
        MinLenDirective
    ],
    //providers: [NoticiasService],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class NewsModule { }
