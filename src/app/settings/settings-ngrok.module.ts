import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { ApplicationSettings } from "@nativescript/core";
import * as Toast from "nativescript-toast";

@Component({
    selector: "settings-ngrok",
    moduleId: module.id,
    template: `
    <FlexboxLayout flexDirection="row">
        <TextField #url="ngModel" [(ngModel)]="textFieldValue" 
            hint="Ingresar url de Nrok..." required minlen="4">
        </TextField>
        <Label *ngIf="url.hasError('required')" text="*"></Label>
    </FlexboxLayout>
    <Button text="Set URL" (tap)="onButtonTap()" *ngIf="url.valid"></Button>
    `
})
export class SettingsNgrokComponent implements OnInit {
    textFieldValue: string = "";
    @Output() search: EventEmitter<string> = new EventEmitter();
    @Input() inicial: string;

    ngOnInit(): void {
        this.textFieldValue = this.inicial;
    }
    doLater(fn) { setTimeout(fn, 1000); }

    onButtonTap(): void {
        console.log("URL: " + this.textFieldValue);
        if (this.textFieldValue.length > 2) {
            ApplicationSettings.setString("url", this.textFieldValue);
            const toastMensaje = Toast.makeText("URL Configurada");
            this.doLater(() => toastMensaje.show());
        }
    }
}
