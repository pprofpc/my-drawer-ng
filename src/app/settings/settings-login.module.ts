import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { ApplicationSettings } from "@nativescript/core";

@Component({
    selector: "settings-login",
    moduleId: module.id,
    template: `
    <FlexboxLayout flexDirection="row">
        <TextField #usuario="ngModel" [(ngModel)]="textFieldValue" 
            hint="Ingresar usuario..." required minlen="4">
        </TextField>
        <Label *ngIf="usuario.hasError('required')" text="*"></Label>
    </FlexboxLayout>
    <Button text="Login" (tap)="onButtonTap()" *ngIf="usuario.valid"></Button>
    `
})
export class SettingsLoginComponent implements OnInit {
    textFieldValue: string = "";
    @Output() search: EventEmitter<string> = new EventEmitter();
    @Input() inicial: string;

    ngOnInit(): void {
        this.textFieldValue = this.inicial;
    }

    onButtonTap(): void {
        console.log(this.textFieldValue);
        if (this.textFieldValue.length > 2) {
            ApplicationSettings.setString("userName", this.textFieldValue);
        }
    }
}
