import { Component, OnInit } from "@angular/core";
import * as Toast from "nativescript-toast";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Application } from "@nativescript/core";
import * as dialogs from "@nativescript/core/ui/dialogs";
import { ApplicationSettings } from "@nativescript/core";

@Component({
    selector: "Settings",
    templateUrl: "./settings.component.html",
    styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {
    changeFlag: boolean = ApplicationSettings.getBoolean("estaLogueado");
    usuario: string = ApplicationSettings.getString("userName");
    url: string = ApplicationSettings.getString("url");

    constructor() {
        // Use the component constructor to inject providers.
    }

    doLater(fn) { setTimeout(fn, 1000); }

    ngOnInit(): void {
        /*
    this.doLater(() =>
            dialogs.action("Mensaje", "Cancelar!", ["Opcion1", "Opcion2"])
                    .then((result) => {
                                        console.log("resultado: " + result);
                                        if (result === "Opcion1") {
                                            this.doLater(() =>
                                                dialogs.alert({
                                                    title: "Titulo 1 ",
                                                    message: "mje 1",
                                                    okButtonText: "btn 1"
                                                }).then(()  => console.log("Cerrado 1!")));
                                        } else if (result === "Opcion2") {
                                            this.doLater(() =>
                                                dialogs.alert({
                                                    title: "Titulo 2",
                                                    message: "mje 2",
                                                    okButtonText: "btn 2"
                                                }).then(() => console.log("Cerrado 2!")));
                                        }
        }));
        */
       const toastMensaje = Toast.makeText("Te invito a identificarte", "short");
       this.doLater(() => toastMensaje.show());
    }

    setChangeFlag(b: boolean): boolean{
        this.changeFlag = b;
        return b;
    }

    getChangeFlag(): boolean{
        return this.changeFlag;
    }

    onButtonTap(): void{
        if (this.getChangeFlag()){
            this.setChangeFlag(false);
            ApplicationSettings.setBoolean("estaLogueado", false);
        }
        else {
            this.setChangeFlag(true);
            ApplicationSettings.setBoolean("estaLogueado", true);
        }
        console.log("Bandera: "+this.getChangeFlag());
    }

    onButtonTapUrl(): void {
        if (this.getChangeFlag()) {
            this.setChangeFlag(false);
            ApplicationSettings.setBoolean("estaLogueado", false);
        }
        else {
            this.setChangeFlag(true);
            ApplicationSettings.setBoolean("estaLogueado", true);
        }
        console.log("Bandera: " + this.getChangeFlag());
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>Application.getRootView();
        sideDrawer.showDrawer();
    }
}
