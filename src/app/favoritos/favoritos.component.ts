import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Application } from "@nativescript/core";
import { NoticiasService } from "../domain/noticias.service";
import { ApplicationSettings } from "@nativescript/core";
import { Router } from "@angular/router";

@Component({
  selector: 'ns-votos',
  templateUrl: './favoritos.component.html',
  styleUrls: ['./favoritos.component.css']
})
export class FavoritosComponent implements OnInit {
  listaFavoritos: string [];

  constructor(public noticias: NoticiasService, private router: Router) {
    this.noticias.getFavoritos().then((f: any)=>{
      console.log("Favoritos: "+JSON.stringify(f));
      this.listaFavoritos=f;
    }, (e) => {
      console.log("Error en getFavoritos: "+ e);
    });
   }

  ngOnInit(): void {
  }

  onNavItemTap(navItemRoute: string, y: string): void {
    ApplicationSettings.setString("sePide", y);
    this.router.navigate([navItemRoute]);
  }

    onDrawerButtonTap(): void {
      const sideDrawer = <RadSideDrawer>Application.getRootView();
        sideDrawer.showDrawer();
    }
}
